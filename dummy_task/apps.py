from django.apps import AppConfig


class DummyTaskConfig(AppConfig):
    name = 'dummy_task'
