from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

# Cuboid model
class Cuboid(models.Model):
    length = models.FloatField(null=True, blank=False)
    width = models.FloatField(null=True, blank=False)
    height = models.FloatField(null=True, blank=False)
    area = models.FloatField(null=True, blank=False)
    volume = models.FloatField(null=True, blank=False)
    created_by = models.ForeignKey(User, null=True, blank=False, editable=False, on_delete=models.SET_NULL)
    # Update when record is created
    created_on = models.DateTimeField(auto_now_add=True)
    # update everytime when recored is updated
    last_updated = models.DateTimeField(auto_now=True)


class Store(models.Model):
    employee = models.ManyToManyField(User)
    box = models.ManyToManyField(Cuboid)

@receiver(post_save, sender=User)
def create_token(sender, instance,**kwargs):
    token, created = Token.objects.get_or_create(user=instance)

post_save.connect(create_token, sender=User)
