from rest_framework import serializers
from .models import Cuboid





# Serializer for Update Create and List
class CuboidCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cuboid
        fields = [
            'id',
            'length',
            'width',
            'height',
            
        ]

class CuboidListSerializerUser(serializers.ModelSerializer):
   
    class Meta:
        model = Cuboid
        fields = [
            'id',
            'length',
            'width',
            'height',
            'area',
            'volume',
        ]

      