from rest_framework import permissions


# permission to check if user has staff access or not
class IsStaffPermission(permissions.BasePermission):
    """
    Permission check for staff user.
    """

    def has_permission(self, request, view):
        return request.user.is_staff
