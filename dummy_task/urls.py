from django.urls import path
from . import views

# apis for CRUD
urlpatterns = [
    path('create/', views.CuboidCreateAPIView.as_view(), name='create'),
    path('update/<pk>/', views.UpdateCuboidAPIView.as_view(), name='update'),
    path('list/', views.BoxList.as_view(), name='list-box'),
    path('list-my-boxes/', views.ListMyboxes.as_view(), name='list-my-box'),
    path('delete/<pk>/', views.DeleteBox.as_view(), name='delete'),

]