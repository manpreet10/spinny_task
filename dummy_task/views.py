from django.shortcuts import render
from rest_framework import generics
from .models import Cuboid
from rest_framework.response import Response
from django.db.models import Q
from django.utils import timezone
from datetime import datetime, timedelta
from .serializers import *
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import  IsAuthenticated
from .permissions import IsStaffPermission
from spinny import settings
from django_filters.rest_framework import DjangoFilterBackend


# Create your views here.
# create API
class CuboidCreateAPIView(generics.CreateAPIView):
    serializer_class = CuboidCreateSerializer
    queryset = Cuboid.objects.all()
    # only staff members can create BOX so permission check is added for staff
    permission_classes = [IsStaffPermission, IsAuthenticated]
    # token authentication provided by DRF to authenticate
    authentication_classes = [TokenAuthentication]

    def create(self, request, *args, **kwargs):
        try:
            # getting the data send by user
            _data = request.data.copy()
            # calculate the no of boxes created in a week 
            d = datetime.today() - timedelta(days=7)
            count = Cuboid.objects.filter(created_by=request.user,created_on__gte =d).count()
            overall_count = Cuboid.objects.filter(created_on__gte =d).count()

            # will return failed if no exceeds///it restrict user from creating boxes out of range
            if overall_count+1 > settings.OVERALL_BOX_CREATE_LIMIT:
                return Response(
                    {"message": f"Cannot create more than {settings.OVERALL_BOX_CREATE_LIMIT} box in a week", "status": 400, "statusType": "failed"})

            if count+1 > settings.BOX_CREATE_LIMIT:
                return Response(
                    {"message": f"Cannot create more than {settings.BOX_CREATE_LIMIT} box in a week", "status": 401, "statusType": "failed"})

            length = float(_data['length'])
            width = float(_data['width'])
            height = float(_data['height'])
            # formula to get area and volume
            area = 2 * (length * width + width * height + length * height)
            volume = length * width * height
               
            # area cannot be greater then benchmark
            if area > settings.AVERAGE_AREA:
                return Response({"message": f"Area cannot exceed {settings.AVERAGE_AREA}", "status": 400, "statusType": "failed"})

            # volume cannot be greater then benchmark
            if volume > settings.AVERAGE_VOLUME:
                return Response({"message": f"Volume cannot exceed {settings.AVERAGE_VOLUME}", "status": 400, "statusType": "failed"})

            serializer = self.get_serializer(data=_data)
            # checking if there are errors in data
            if serializer.is_valid():
                obj = self.perform_create(serializer, area, volume)
                   
                return Response({"message": "Success",'data':serializer.data, "status": 200, "statusType": "success"})
            else:
                return Response({"message": serializers.errors, "status": 401, "statusType": "failed"})
        except Exception as e:
            # if any error occur then it will return failed
            return Response({"message": str(e), "status": 400, "statusType": "failed"})

    def perform_create(self, serializer, area, volume):
        # saving area and volume of box
        return serializer.save(created_by=self.request.user, area=area, volume=volume)


# update the cuboid
class UpdateCuboidAPIView(generics.UpdateAPIView):
    lookup_field = 'pk'
    serializer_class = CuboidCreateSerializer
    queryset = Cuboid.objects.all()
    # only staff can update the box..
    permission_classes = [IsStaffPermission, IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    def put(self, request, *args, **kwargs):
        try:
            queryset = self.get_queryset()
            instance = self.get_object()
            data = request.data
            serializer = self.get_serializer(instance, data=data)

            if serializer.is_valid():
                serializer.save()
                return Response({"status": 200, 'data': serializer.data, "statusType": "success"})
            else:
                return Response({"status": 401, "message": serializer.errors, "statusType": "failed"})
        except Exception as e:
            return Response({"message": str(e), "status": 400, "statusType": "failed"})

# delete api
class DeleteBox(generics.DestroyAPIView):
    queryset = Cuboid.objects.all()
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            instance.delete()
            return Response({"message": "Deleted Successfully", "status": 200, "statusType": "success"})
        except Exception as e:
            return Response({"message": str(e), "status": 400, "statusType": "failed"})





# ALL boxes api
class BoxList(generics.ListAPIView):
    authentication_classes = [TokenAuthentication]
    serializer_class = CuboidListSerializerUser
    queryset = Cuboid.objects.all()
    permission_classes = [IsAuthenticated]

    filter_backends = (DjangoFilterBackend,)
    # filters we can add in the query to get required data
    filterset_fields = {
        'length': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'width': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'height': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'area': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'volume': ['gte', 'lte', 'exact', 'gt', 'lt']
    }

    def get(self, request, *args, **kwargs):
        try:
            queryset = Cuboid.objects.all()

            if request.user.is_staff:
                filter_backends = self.filter_queryset(queryset)
                serializer = CuboidListSerializerUser(filter_backends, many=True, context={'request': request})
            else:
                serializer = self.get_serializer(queryset, many=True, context={'request': request})
            data = { "statusType": "success", "status": 200, "detail": {"boxes": serializer.data}}
            return Response(data)
        except Exception:
            return Response({"message": "something bad happened", "statusType": "failed", "status": 400})


# API to get my created  boxes
class ListMyboxes(generics.ListAPIView):
    authentication_classes = [TokenAuthentication]
    # only staff members can see his created boxes
    permission_classes = [IsStaffPermission, IsAuthenticated]
    serializer_class = CuboidListSerializerUser
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'length': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'width': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'height': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'area': ['gte', 'lte', 'exact', 'gt', 'lt'],
        'volume': ['gte', 'lte', 'exact', 'gt', 'lt'],
    }

    def get(self, request, *args, **kwargs):
        try:
            queryset = Cuboid.objects.filter(created_by=self.request.user)
            filter_backends = self.filter_queryset(queryset)
            serializer = self.get_serializer(filter_backends, many=True, context={'request': request})
            data = {"statusType": "success", "status": 200, "detail": {"boxes": serializer.data}}
            return Response(data)
        except Exception as e:
            return Response({"message":str(e), "statusType": "failed", "status": 400})